package com.twuc.tdd;

import java.util.ArrayList;
import java.util.List;

public class BowlingGame {
    private final static int STRIKE_SCORE = 10;
    private final static int SPARE_SCORE = 10;
    public List<Frame> frames = new ArrayList<>();
    private int score;
    private List<Frame> bonusFrames = new ArrayList<>();

    public void addFrames(Frame frame) {
        frames.add(frame);
    }

    public List<Frame> getBonusFrames() {
        return bonusFrames;
    }

    public void addBonusFrames(Frame frame) {
        bonusFrames.add(frame);
    }

    public void calculateScore() {
        handlePrev8Frames();
        handleLast2Frames();
        this.score = frames.stream().map(frame -> {
            frame.calculateScore(0);
            return frame.getScore();
        }).reduce(Integer::sum).orElse(0);
    }

    private void handleLast2Frames() {
        if (frames.size() == 10) {
            Frame lastSecondFrame = frames.get(8);
            Frame lastFrame = frames.get(9);
            if (lastFrame.getFirstThrow() != STRIKE_SCORE && lastFrame.getScore() == SPARE_SCORE) {
                lastFrame.calculateScore(bonusFrames.get(0).getFirstThrow());
            }
            if (lastFrame.getFirstThrow() == STRIKE_SCORE) {
                if (bonusFrames.get(0).getFirstThrow() == STRIKE_SCORE) {
                    lastFrame.calculateScore(bonusFrames.get(0).getScore());
                    lastFrame.calculateScore(bonusFrames.get(1).getScore());
                } else {
                    lastFrame.calculateScore(bonusFrames.get(0).getScore());
                }
            }
            if (lastSecondFrame.getFirstThrow() == STRIKE_SCORE) {
                if (lastFrame.getFirstThrow() != STRIKE_SCORE) {
                    lastSecondFrame.calculateScore(lastFrame.getFirstThrow() + lastFrame.getSecondThrow());
                } else {
                    lastSecondFrame.calculateScore(lastFrame.getFirstThrow());
                    lastSecondFrame.calculateScore(bonusFrames.get(0).getFirstThrow());
                }
            }
        }
    }

    private void handlePrev8Frames() {
        for (int i = 0; i < frames.size() - 1; i++) {
            Frame preFrame = frames.get(i);
            final Frame nextFrame = frames.get(i + 1);
            if (preFrame.getFirstThrow() != STRIKE_SCORE && preFrame.getScore() == SPARE_SCORE) {
                preFrame.calculateScore(nextFrame.getFirstThrow());
            }
            if (preFrame.getFirstThrow() == STRIKE_SCORE && i < 8) {
                if (nextFrame.getFirstThrow() == STRIKE_SCORE) {
                    preFrame.calculateScore(nextFrame.getFirstThrow());
                    preFrame.calculateScore(frames.get(i + 2).getFirstThrow());
                } else {
                    preFrame.calculateScore(nextFrame.getFirstThrow());
                    preFrame.calculateScore(nextFrame.getSecondThrow());
                }
            }
        }
    }

    public int getScore() {
        return this.score;
    }
}
