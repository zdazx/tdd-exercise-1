package com.twuc.tdd;

public class Frame {
    private int firstThrow;
    private int secondThrow;
    private int score;

    public Frame() {
    }

    public Frame(int firstThrow) {
        this.firstThrow = firstThrow;
        this.score = this.firstThrow;
    }

    public Frame(Integer firstThrow, Integer secondThrow) {
        this.firstThrow = firstThrow;
        this.secondThrow = secondThrow;
        this.score = this.firstThrow + this.secondThrow;
    }

    public int getSecondThrow() {
        return secondThrow;
    }

    public void calculateScore(int bonusSore) {
        this.score += bonusSore;
    }

    public int getScore() {
        return this.score;
    }

    public int getFirstThrow() {
        return this.firstThrow;
    }

}
