package com.twuc.tdd;

import org.junit.Before;
import org.junit.Test;

import java.util.stream.Stream;

import static org.junit.Assert.assertEquals;

public class TDDTest {
    Frame frame;
    BowlingGame bowlingGame;

    @Before
    public void setUp() {
        bowlingGame = new BowlingGame();
    }

    @Test
    public void should_return_0_when_no_throws() {
        int score = bowlingGame.getScore();
        assertEquals(0, score);
    }

    @Test
    public void should_return_one_pins_number_when_throw_once() {
        frame = new Frame(2);
        bowlingGame.addFrames(frame);
        bowlingGame.calculateScore();
        int score = bowlingGame.getScore();
        assertEquals(2, score);
    }

    @Test
    public void should_return_sum_of_two_throws_when_throws_twice() {
        frame = new Frame(3, 5);
        bowlingGame.addFrames(frame);
        bowlingGame.calculateScore();
        int score = bowlingGame.getScore();
        assertEquals(8, score);
    }

    @Test
    public void should_return_sum_of_ten_frames_without_strike_and_spare() {
        Stream.iterate(0, n -> n + 1)
                .limit(10)
                .forEach(n -> bowlingGame.addFrames(new Frame(3, 5)));
        bowlingGame.calculateScore();
        assertEquals(80, bowlingGame.getScore());
    }

    @Test
    public void should_calculate_score_when_first_frame_is_spare() {
        bowlingGame.addFrames(new Frame(4, 6));
        Stream.iterate(0, n -> n + 1)
                .limit(9)
                .forEach(n -> bowlingGame.addFrames(new Frame(3, 6)));
        bowlingGame.calculateScore();
        assertEquals(94, bowlingGame.getScore());
    }

    @Test
    public void should_calculate_score_when_ten_frames_with_spare_but_not_last() {
        bowlingGame.addFrames(new Frame(4, 6));
        bowlingGame.addFrames(new Frame(2, 8));
        Stream.iterate(0, n -> n + 1)
                .limit(8)
                .forEach(n -> bowlingGame.addFrames(new Frame(3, 6)));
        bowlingGame.calculateScore();
        assertEquals(97, bowlingGame.getScore());
    }

    @Test
    public void should_calculate_score_when_ten_frames_with_last_spare() {
        Stream.iterate(0, n -> n + 1)
                .limit(9)
                .forEach(n -> bowlingGame.addFrames(new Frame(3, 6)));
        bowlingGame.addFrames(new Frame(4, 6));
        bowlingGame.addBonusFrames(new Frame(4));
        bowlingGame.calculateScore();
        assertEquals(95, bowlingGame.getScore());
    }

    @Test
    public void should_calculate_score_when_ten_frames_with_first_strike() {
        bowlingGame.addFrames(new Frame(10));
        Stream.iterate(0, n -> n + 1)
                .limit(9)
                .forEach(n -> bowlingGame.addFrames(new Frame(3, 6)));
        bowlingGame.calculateScore();
        assertEquals(100, bowlingGame.getScore());
    }

    @Test
    public void should_calculate_score_when_ten_frames_with_middle_strike() {
        bowlingGame.addFrames(new Frame(10));
        bowlingGame.addFrames(new Frame(10));
        Stream.iterate(0, n -> n + 1)
                .limit(6)
                .forEach(n -> bowlingGame.addFrames(new Frame(3, 6)));
        bowlingGame.addFrames(new Frame(10));
        bowlingGame.addFrames(new Frame(10));
        bowlingGame.addBonusFrames(new Frame(4,5));
        bowlingGame.calculateScore();
        assertEquals(139, bowlingGame.getScore());
    }

    @Test
    public void should_calculate_score_when_ten_frames_with_last_strike() {
        Stream.iterate(0, n -> n + 1)
                .limit(9)
                .forEach(n -> bowlingGame.addFrames(new Frame(3, 6)));
        bowlingGame.addFrames(new Frame(10));
        bowlingGame.addBonusFrames(new Frame(4,5));
        bowlingGame.calculateScore();
        assertEquals(100, bowlingGame.getScore());
    }

    @Test
    public void should_return_300_when_ten_frame_is_all_strike() {
        Stream.iterate(0, n -> n + 1)
                .limit(10)
                .forEach(n -> bowlingGame.addFrames(new Frame(10)));
        bowlingGame.addBonusFrames(new Frame(10));
        bowlingGame.addBonusFrames(new Frame(10));
        bowlingGame.calculateScore();
        assertEquals(300, bowlingGame.getScore());
    }
}
